/*global angular, window */
/*jslint node: true */
'use strict';

var hangTheManControllers = angular.module('hangTheManControllers', []);

hangTheManControllers.controller('hangTheManCtrl', ['$scope', '$routeParams', 'Games', 'Play',
    function ($scope, $routeParams, Games, Play) {
        $scope.gameId = $routeParams.id;
        $scope.game = Games.query({
            gameId: $scope.gameId
        });
        $scope.newGuess = '';
        $scope.playable = $scope.game.misses < 5 && $scope.game.puzzle.indexOf('_') < 0;
        $scope.submit = function ($event) {
            if ($scope.game.guesses.indexOf($scope.newGuess) < 0) {
                $scope.game = Play.query({
                    gameId: $scope.gameId,
                    newGuess: $scope.newGuess
                });
            }
            $scope.newGuess = '';
        };
    }]);

hangTheManControllers.controller('welcomeCtrl', ['$scope', 'AllGames',
    function ($scope, AllGames) {
        $scope.games = AllGames.query();
    }]);

hangTheManControllers.controller('newGameCtrl', ['$scope', 'NewGame',
    function ($scope, NewGame) {
        $scope.submit = function ($event) {
            NewGame.query({
                "newPuzzle": $scope.newPuzzle
            });
            $scope.newPuzzle = '';
        };
    }]);
/*global angular */
/*jslint node: true */
'use strict';

var hangTheManApp = angular.module('hangTheManApp', [
        'ngRoute',
        'hangTheManControllers',
        'hangTheManServices'
    ]);

hangTheManApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.when('/index.html', {
            templateUrl: 'partials/welcome.html',
            controller: 'welcomeCtrl'
        }).when('/play', {
            templateUrl: 'partials/hang-the-man-body.html',
            controller: 'hangTheManCtrl'
        }).when('/create', {
            templateUrl: 'partials/create-game.html',
            controller: 'newGameCtrl'
        }).otherwise({
            redirectTo: '/index.html'
        });
    }]);
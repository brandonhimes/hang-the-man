/*global angular */
/*jslint node: true */
'use strict';

var hangTheManServices = angular.module('hangTheManServices', ['ngResource']);

hangTheManServices.factory('Games', ['$resource',
    function ($resource) {
        return $resource('http://brandolinds.asuscomm.com:3000/games/:gameId', {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }]);

hangTheManServices.factory('AllGames', ['$resource',
    function ($resource) {
        return $resource('http://brandolinds.asuscomm.com:3000/games', {}, {
            query: {
                method: 'GET',
                isArray: true
            }
        });
    }]);

hangTheManServices.factory('Play', ['$resource',
    function ($resource) {
        return $resource('http://brandolinds.asuscomm.com:3000/games/play/:gameId/:newGuess', {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }]);

hangTheManServices.factory('NewGame', ['$resource',
    function ($resource) {
        return $resource('http://brandolinds.asuscomm.com:3000/games/create/:newPuzzle', {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    }]);